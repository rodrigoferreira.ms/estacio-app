package com.diogosoares.estacioapp.utils;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.diogosoares.estacioapp.daos.UserDao;
import com.diogosoares.estacioapp.models.User;

/**
 * Created by diogosoares on 28/03/18.
 */

public class DbOperations {

    public interface DBOperationsCallBack {
        void userSaverd(Boolean success);

        void userUpdated(Boolean success);

        void userDeleted(Boolean success);
    }

    private static final String TAG = DbOperations.class.getName();

    private Context context;
    private DBOperationsCallBack callback;

    public DbOperations(Context context) {
        this.context = context;
    }

    public void saveUser(User user, DBOperationsCallBack callback){
        this.callback = callback;
        new saveUserTask().execute(user);
    }

    private class saveUserTask extends AsyncTask<User, Void, Void> {
        @Override
        protected Void doInBackground(User... users) {
             try {
                UserDao userDao = AppDatabase.getInstance(context).userDao();
                userDao.insertAll(users);
                if (callback != null) {
                    callback.userSaverd(true);
                }
            } catch (Exception e) {
                Log.e("saveUserTask", e.getMessage());
                if (callback != null) {
                    callback.userSaverd(false);
                }
            }
            return null;
        }
    }

}
