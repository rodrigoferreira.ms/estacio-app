package com.diogosoares.estacioapp.activities;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.diogosoares.estacioapp.R;
import com.diogosoares.estacioapp.models.User;
import com.diogosoares.estacioapp.utils.DbOperations;

public class MainActivity extends AppCompatActivity {
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        User user = new User();
        user.setEncryptedPassword("adlkjasda");
        user.setUsername("asdlkajsd");
        DbOperations dbOperations = new DbOperations(this);
        startProgressDialog();
        dbOperations.saveUser(user, new DbOperations.DBOperationsCallBack() {
            @Override
            public void userSaverd(Boolean success) {
                stopProgressDialog();
            }

            @Override
            public void userUpdated(Boolean success) {

            }

            @Override
            public void userDeleted(Boolean success) {

            }
        });
    }

    private void startProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Carregando");
        progressDialog.show();
    }

    private void stopProgressDialog() {
        progressDialog.dismiss();
    }

    public void login(View view) {


        EditText username = (EditText) findViewById(R.id.username);
        EditText password = (EditText) findViewById(R.id.password);

        String user = username.getText().toString();
        String pass = password.getText().toString();

    }
}
